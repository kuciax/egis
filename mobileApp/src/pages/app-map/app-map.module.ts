import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AppMapPage } from './app-map';

@NgModule({
  declarations: [
    AppMapPage,
  ],
  imports: [
    IonicPageModule.forChild(AppMapPage),
  ],
})
export class AppMapPageModule {}
