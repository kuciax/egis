import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Position } from "../model/postion";

@Injectable()
export class HttpService extends HttpClient {

    sendPosition(url: string, position: Position): Observable<any> {
        return super.post(url,position);
    }
}