package com.predictive.lambda.domain;

public enum LightConditionType {
	DAYLIGHT("1"),
	DARKNESS_LIGHTS_LIT("4"),
	DARKNESS_LIGHTS_UNLIT("5"),
	DARKNESS_NO_LIGHTNING("6"),
	DARKNESS_LIGHTNING_UNKNOWN("7"),
	UNKNOWN_CONDITION("-1");
	
	private final String lightningCode;

	private LightConditionType(String lightningCode) {
		this.lightningCode = lightningCode;
	}

	public String getLightningCode() {
		return lightningCode;
	}
	
}
