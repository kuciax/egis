package com.predictive.lambda.domain;

public enum WeatherType {

	/**
	 * High wind starts from 39km/hour
	 */
	FINE_NO_HIGH_WINDS("1"),
	RAINING_NO_HIGH_WINDS("2"),
	SNOWING_NO_HIGH_WINDS("3"),
	FINE_HIGH_WINDS("4"),
	RAINING_HIGH_WINDS("5"),
	SNOWING_HIGH_WINDS("6"),
	FOG_OR_MIST("7"),
	OTHER("8"),
	UNKNOWN("9"),
	DATA_MISSING_OR_OUT_OF_RANGE("-1");
	
	private final String weatherCode;

	private WeatherType(String weatherCode) {
		this.weatherCode = weatherCode;
	}

	public String getWeatherCode() {
		return weatherCode;
	}
	
}
