package com.predictive.lambda;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.machinelearning.AmazonMachineLearning;
import com.amazonaws.services.machinelearning.AmazonMachineLearningClientBuilder;
import com.amazonaws.services.machinelearning.model.GetMLModelRequest;
import com.amazonaws.services.machinelearning.model.GetMLModelResult;
import com.amazonaws.services.machinelearning.model.PredictRequest;
import com.amazonaws.services.machinelearning.model.PredictResult;
import com.predictive.lambda.domain.AccidentType;
import com.predictive.lambda.domain.LightConditionType;
import com.predictive.lambda.domain.LocationReceivedTO;
import com.predictive.lambda.domain.ResponseWeatherApiTO;
import com.predictive.lambda.domain.WeatherType;
import com.predictive.lambda.exceptions.LambdaFunctionHandlerException;
import com.predictive.lambda.helper.LambdaHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class LambdaFunctionHandler implements RequestHandler<LocationReceivedTO, String> {

	private static final Logger LOGGER = Logger.getLogger(LambdaFunctionHandler.class.getName());

	private static final String OPEN_WEATHER_API_KEY;
	private static final String OPEN_WEATHER_API_URL;

	private static final float PREDICTION_MINIMAL_PROBABILITY_THRESHOLD = 0.15f;

	public static final String ACCIDENTS_CATEGORY_PREDICTION_MODEL_ID;
	
	public static final String ACCIDENTS_PROBABILITY_PREDICTION_MODEL_ID;
	
	static {
		OPEN_WEATHER_API_KEY = Objects.requireNonNull(System.getenv("Open_Weather_Api_Key"));
		OPEN_WEATHER_API_URL = Objects.requireNonNull(System.getenv("Open_Weather_Api_Url"));
		ACCIDENTS_CATEGORY_PREDICTION_MODEL_ID = Objects.requireNonNull(System.getenv("Accidents_Category_Prediction_Model_Id"));
		ACCIDENTS_PROBABILITY_PREDICTION_MODEL_ID = Objects.requireNonNull(System.getenv("Accidents_Probability_Prediction_Model_Id"));
	}

	private final ObjectMapper objectMapper;
	private final Map<String, AccidentType> accidentTypeToName;

	public LambdaFunctionHandler() {
		this.objectMapper = new ObjectMapper();
		this.accidentTypeToName = Stream.of(AccidentType.values())
			.collect(Collectors.toMap(AccidentType::name, accidentType -> accidentType));
	}

	@Override
	public String handleRequest(LocationReceivedTO input, Context context) {
		String urlString = OPEN_WEATHER_API_URL + "?units=metric&lat=" + input.getLat() + "&lon=" + input.getLng()
				+ "&APPID=" + OPEN_WEATHER_API_KEY;

		ResponseWeatherApiTO response = null;
		try {
			URL url = new URL(urlString);
			HttpURLConnection connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setConnectTimeout(8000);
			connection.setReadTimeout(8000);
			int responseCode = connection.getResponseCode();
			if (responseCode != HttpURLConnection.HTTP_OK) {
				generateErrorPayload("Failed to retrieve data from Weather API.", context.getAwsRequestId());
			}
			response = objectMapper.readValue(connection.getInputStream(), ResponseWeatherApiTO.class);
			if (response.getWeather().isEmpty()) {
				generateErrorPayload("Unknown data retrieved from Weather API.", context.getAwsRequestId());
			}
		} catch (IOException e) {
			generateErrorPayload("Service Weather API Temporarily unavailable. " + e.getMessage(),
					context.getAwsRequestId());
		}
		
		PredictResult predictResultProbability = this.predictAccidentProbability(response);
		if (predictResultProbability.getPrediction().getPredictedLabel().equals("true")) {
			PredictResult predictionResultCategory = this.predictAccidentCategory(response);
			return this.parsePredictionResult(predictionResultCategory);
		}

		return AccidentType.NO_ACCIDENT.getMp3FileName();
	}

	private String generateErrorPayload(String exceptionMessage, String requestId)
			throws LambdaFunctionHandlerException {
		Map<String, Object> errorPayload = new HashMap<>();
		errorPayload.put("errorType", "InternalServerError");
		errorPayload.put("httpStatus", 500);
		errorPayload.put("requestId", requestId);
		errorPayload.put("message", "An error has occurred. Original exception message: " + exceptionMessage);
		String message;
		try {
			message = this.objectMapper.writeValueAsString(errorPayload);
		} catch (JsonProcessingException e) {
			LOGGER.warning(e.getMessage());
			throw new com.predictive.lambda.exceptions.LambdaFunctionHandlerException(
					exceptionMessage + ". Additional exception: " + e.getMessage());
		}

		throw new LambdaFunctionHandlerException(message);
	}

	private PredictResult predictAccidentCategory(ResponseWeatherApiTO responseWeatherApiTO) {
		AmazonMachineLearning client = AmazonMachineLearningClientBuilder.standard().withRegion(Regions.EU_WEST_1)
				.build();
		String predictEndpoint = getAIEndpointUrl(client, ACCIDENTS_CATEGORY_PREDICTION_MODEL_ID);

		LightConditionType lightConditionType = LambdaHelper.getLightConditionType(
				responseWeatherApiTO.getSys().getSunrise(), responseWeatherApiTO.getSys().getSunset());
		WeatherType weatherType = LambdaHelper.getWeatherType(responseWeatherApiTO.getWeather().get(0).getId(),
				responseWeatherApiTO.getWind().getSpeed());
		DateTime currentDatetime = DateTime.now(DateTimeZone.UTC);
		String time = String.valueOf(currentDatetime.getHourOfDay()) + ":"
				+ String.valueOf(currentDatetime.getMinuteOfHour());
		String dayOfWeek = currentDatetime.dayOfWeek().getAsString();

		Map<String, String> record = this.generateRecordPredictionCategory(responseWeatherApiTO.getCoord().getLat(),
				responseWeatherApiTO.getCoord().getLon(), dayOfWeek, time, lightConditionType, weatherType);

		PredictRequest predictRequest = new PredictRequest()
				.withMLModelId(ACCIDENTS_CATEGORY_PREDICTION_MODEL_ID)
				.withPredictEndpoint(predictEndpoint)
				.withRecord(record);

		return client.predict(predictRequest);
	}
	
	private String getAIEndpointUrl(AmazonMachineLearning client, String modelId) {
		GetMLModelRequest modelRequest = new GetMLModelRequest()
				.withMLModelId(modelId);
		GetMLModelResult model = client.getMLModel(modelRequest);
		return model.getEndpointInfo().getEndpointUrl();
	}

	/**
	 * 
	 * @param input
	 * @param dayOfWeek
	 * @param time               format like 12:12
	 * @param lightConditionType
	 * @param weatherType
	 * @return
	 */
	private Map<String, String> generateRecordPredictionCategory(double latitude, double longitude, String dayOfWeek, String time,
			LightConditionType lightConditionType, WeatherType weatherType) {
		Map<String, String> record = new HashMap<>();
		record.put("Longitude", String.valueOf(longitude));
		record.put("Latitude", String.valueOf(latitude));
		record.put("Day_of_Week", dayOfWeek);
		record.put("Time", time);
		record.put("Light_Conditions", lightConditionType.getLightningCode());
		record.put("Weather_Conditions", weatherType.getWeatherCode());
		record.put("ACCIDENT", "");

		return record;
	}
	
	private PredictResult predictAccidentProbability(ResponseWeatherApiTO responseWeatherApiTO) {
		AmazonMachineLearning client = AmazonMachineLearningClientBuilder.standard().withRegion(Regions.EU_WEST_1)
				.build();
		String predictEndpoint = getAIEndpointUrl(client, ACCIDENTS_PROBABILITY_PREDICTION_MODEL_ID);

		LightConditionType lightConditionType = LambdaHelper.getLightConditionType(
				responseWeatherApiTO.getSys().getSunrise(), responseWeatherApiTO.getSys().getSunset());
		WeatherType weatherType = LambdaHelper.getWeatherType(responseWeatherApiTO.getWeather().get(0).getId(),
				responseWeatherApiTO.getWind().getSpeed());
		DateTime currentDatetime = DateTime.now(DateTimeZone.UTC);
		String hourOfTheDay = String.valueOf(currentDatetime.getHourOfDay());
		String dayOfYear = currentDatetime.dayOfYear().getAsString();

		Map<String, String> record = this.generateRecordPredictionProbability(responseWeatherApiTO.getCoord().getLat(),
				responseWeatherApiTO.getCoord().getLon(), dayOfYear, hourOfTheDay, lightConditionType, weatherType);

		PredictRequest predictRequest = new PredictRequest()
				.withMLModelId(ACCIDENTS_PROBABILITY_PREDICTION_MODEL_ID)
				.withPredictEndpoint(predictEndpoint)
				.withRecord(record);

		return client.predict(predictRequest);
	}
	
	private Map<String, String> generateRecordPredictionProbability(double latitude, double longitude, String dayOfYear, String hourOfTheDay,
			LightConditionType lightConditionType, WeatherType weatherType) {
		Map<String, String> record = new HashMap<>();
		record.put("Longitude", String.valueOf(longitude));
		record.put("Latitude", String.valueOf(latitude));
		record.put("Day_of_year", dayOfYear);
		record.put("Hour_of_the_day", hourOfTheDay);
		record.put("Light_Conditions", lightConditionType.getLightningCode());
		record.put("Weather_Conditions", weatherType.getWeatherCode());

		return record;
	}

	private String parsePredictionResult(PredictResult predictResult) {
		boolean isAccidentProbable = predictResult.getPrediction().getPredictedScores().entrySet()
		.stream()
		.anyMatch(entry -> entry.getValue() >= PREDICTION_MINIMAL_PROBABILITY_THRESHOLD);
		
		if (isAccidentProbable) {
			if (this.accidentTypeToName.containsKey(predictResult.getPrediction().getPredictedLabel())) {
				return this.accidentTypeToName.get(predictResult.getPrediction().getPredictedLabel()).getMp3FileName();
			}
			return AccidentType.UNKNOWN_ACCIDENT.getMp3FileName();
		}
		
		return AccidentType.NO_ACCIDENT.getMp3FileName();
	}

}
