package com.predictive.lambda.domain;

public enum AccidentType {

	PEDESTRIAN("pedestrians.mp3"), 
	CYCLIST("cyclists.mp3"), 
	MOTORCYCLE("motorcyclists.mp3"), 
	SNOWING("snowfall.mp3"), 
	RAINING("rain.mp3"), 
	BAD_WEATHER("badWeatherConditions.mp3"), 
	DARKNESS("darkness.mp3"), 
	NORMAL_ACCIDENT("normalAccident.mp3"), 
	NO_ACCIDENT("noAccident"),
	UNKNOWN_ACCIDENT("unknownAccident");
	
	private final String mp3FileName;

	private AccidentType(String mp3FileName) {
		this.mp3FileName = mp3FileName;
	}

	public String getMp3FileName() {
		return mp3FileName;
	}
	
}
