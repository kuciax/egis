package com.predictive.lambda.helper;

import java.util.Date;
import java.util.logging.Logger;

import com.predictive.lambda.domain.LightConditionType;
import com.predictive.lambda.domain.WeatherType;

public class LambdaHelper {
	
	private static final Logger LOGGER = Logger.getLogger(LambdaHelper.class.getName());

	private LambdaHelper() {
		
	}
	
	/**
	 * Weather codes available on https://openweathermap.org/weather-conditions
	 * @param response
	 * @return
	 */
	public static WeatherType getWeatherType(int weatherCode, double windSpeed) {
		//High wind starts from 39km/hour
		boolean isHighWind = windSpeed > 39.0;
		if (weatherCode >= 200 && weatherCode < 600) {
			if (isHighWind) {
				return WeatherType.RAINING_HIGH_WINDS;
			}
			return WeatherType.RAINING_NO_HIGH_WINDS;
		} else if (weatherCode >= 600 && weatherCode < 700) {
			if (isHighWind) {
				return WeatherType.SNOWING_HIGH_WINDS;
			}
			return WeatherType.SNOWING_NO_HIGH_WINDS;
		} else if (weatherCode >= 700 && weatherCode < 800) {
			return WeatherType.FOG_OR_MIST;
		} else if (weatherCode >= 800 && weatherCode < 900) {
			if (isHighWind) {
				return WeatherType.FINE_HIGH_WINDS;
			}
			return WeatherType.FINE_NO_HIGH_WINDS;
		}
		
		return WeatherType.OTHER;
	}
	
	public static LightConditionType getLightConditionType(int unixDateSecondsSunrise, int unixDateSecondsSunset) {
		if (isDark(unixDateSecondsSunrise, unixDateSecondsSunset)) {
			LOGGER.info("DARKNESS_LIGHTS_LIT");
			return LightConditionType.DARKNESS_LIGHTS_LIT;
		}
		
		LOGGER.info("DAYLIGHT");
		return LightConditionType.DAYLIGHT;
	}
	
	private static boolean isDark(int unixDateSecondsSunrise, int unixDateSecondsSunset) {
		Date sunriseDate = new Date(unixDateSecondsSunrise * 1000);
		Date sunsetDate = new Date(unixDateSecondsSunset * 1000);
		Date currentDate = new Date();
		if (currentDate.before(sunsetDate) && currentDate.after(sunriseDate)) {
			return true;
		}
		
		return false;
	}
}
