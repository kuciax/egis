package com.predictive.lambda;

import java.io.IOException;
import java.util.Date;
import java.util.logging.Logger;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.machinelearning.AmazonMachineLearning;
import com.amazonaws.services.machinelearning.AmazonMachineLearningClientBuilder;
import com.amazonaws.services.machinelearning.model.GetMLModelRequest;
import com.amazonaws.services.machinelearning.model.GetMLModelResult;
import com.predictive.lambda.domain.LocationReceivedTO;

/**
 * A simple test harness for locally invoking your Lambda function handler.
 */
public class LambdaFunctionHandlerTest {

	private static final Logger LOGGER = Logger.getLogger(LambdaFunctionHandlerTest.class.getName());
	
	private static LocationReceivedTO input;

    @BeforeClass
    public static void createInput() throws IOException {
        // TODO: set up your sample input object here.
        input = new LocationReceivedTO();
        input.setLat("53");
        input.setLng("18");
    }

    private Context createContext() {
        TestContext ctx = new TestContext();

        // TODO: customize your context here if needed.
        ctx.setFunctionName("Your Function Name");

        return ctx;
    }

    @Test
    public void testLambdaFunctionHandler() {
        LambdaFunctionHandler handler = new LambdaFunctionHandler();
        Context ctx = createContext();

        String output = handler.handleRequest(input, ctx);
        LOGGER.info("Returned output: " + output);
        // TODO: validate output here if needed.
        Assert.assertNotNull(output);
    }
    
    @Test
    public void testMapping() {
    	// given
    	Date sunsetDate = new Date(1538152120L * 1000);
    	LOGGER.info(sunsetDate.toString());
    	AmazonMachineLearning client = AmazonMachineLearningClientBuilder.standard()
    			.withRegion(Regions.EU_WEST_1)
    			.build();
    	GetMLModelRequest modelRequest = new GetMLModelRequest().withMLModelId(LambdaFunctionHandler.ACCIDENTS_CATEGORY_PREDICTION_MODEL_ID);
    	GetMLModelResult model = client.getMLModel(modelRequest);
        String predictEndpoint = model.getEndpointInfo().getEndpointUrl();
        LOGGER.info(predictEndpoint);
    	//client.ge
    	
    			//new AmazonMachineLearningClient();
    	
    	
    	// when
    	
    	
    	// then
    }
}
